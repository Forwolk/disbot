package ru.forwolk.discord.bot;

public interface IViolationService {
    void ban (long guildId, long userId, String reason, long actor, long timeInSec);
    void ban (long guildId, long userId, String reason, long actor);
    void mute (long guildId, long userId, String reason, long actor, long timeInSec);
    void unban (long guildId, long userId, long actor, String reason);
    void unmute (long guildId, long userId, long actor, String reason);
    void kick (long guildId, long userId, long actor, String reason);
    default void unban (long guildId, long userId, long actor) {
        unban(guildId, userId, actor, "");
    }
    default void unmute (long guildId, long userId, long actor) {
        unmute(guildId, userId, actor, "");
    }
    default void kick (long guildId, long userId, long actor) {
        kick(guildId, userId, actor, "");
    }
    boolean isMuted (long guildId, long userId);
    boolean isBanned (long guildId, long userId);
}
