package ru.forwolk.discord.bot;

import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IUser;

public interface IPermissionService {

    boolean hasPermission (IGuild guild, long userId, String permission);
    boolean hasPermission (IGuild guild, IUser user, String permission);
}
