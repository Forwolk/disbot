package ru.forwolk.discord.bot;

import lombok.Setter;

public enum  Message {
    TOO_LOW_ARGUMENTS("Too low arguments", "Слишком мало аргументов"),
    INVALID_TIME_FORMAT("Invalid time format", "Неверный формат времени"),
    USER_KICKED("Moderator {0} has kicked {1}", "{0} кикнул с сервера {1}"),
    USER_BANNED("Moderator {0} has banned {1}", "{0} забанил {1}"),
    USER_MUTED("Moderator {0} has muted {1}", "{0} запретил писать в чат {1}"),
    USER_UNBANNED("Moderator {0} has unbanned {1}", "{0} разбанил {1}"),
    INVALID_NUMBER("Invalid number", "Некорректное число"),
    USER_UNMUTED("Moderator {0} has unmuted {1}", "{0} разрешил писать в чат {1}");


    private final String enMessage;
    private final String ruMessage;
    @Setter
    private static boolean translated = false;

    Message(String enMessage, String ruMessage) {
        this.ruMessage = ruMessage;
        this.enMessage = enMessage;
    }

    public String getMessage() {
        if (translated)
            return ruMessage;
        else
            return enMessage;
    }
}
