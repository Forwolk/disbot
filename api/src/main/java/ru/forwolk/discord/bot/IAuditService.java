package ru.forwolk.discord.bot;

import java.util.HashMap;
import java.util.Map;

public interface IAuditService {

    default void audit (long userId, String event) {
        audit(userId, event, new HashMap<>());
    }
    void audit (long userId, String event, Map<String, String> data);
}
