package ru.forwolk.discord.bot;

import sx.blah.discord.handle.obj.IChannel;

import java.awt.*;

public interface IOutputManager {
    void print (IChannel channel, String message);
    void print (IChannel channel, String message, Color color);
    default void print (IChannel channel, Message message) {
        print(channel, message.getMessage());
    }
    default void print (IChannel channel, Message message, Color color) {
        print(channel, message.getMessage(), color);
    }
}
