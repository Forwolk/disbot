package ru.forwolk.discord.bot.impl;

import ru.forwolk.discord.bot.IPermissionService;
import ru.forwolk.discord.bot.Permissions;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IUser;

import java.util.EnumSet;

public class PermissionServiceImpl implements IPermissionService {
    @Override
    public boolean hasPermission(IGuild guild, long userId, String permission) {
        return hasPermission(guild, guild.getUserByID(userId), permission);
    }

    @Override
    public boolean hasPermission(IGuild guild, IUser user, String permission) {
        EnumSet<sx.blah.discord.handle.obj.Permissions> permissions = user.getPermissionsForGuild(guild);
        switch (permission) {
            case Permissions.BAN_PERMISSION:
                return permissions.contains(sx.blah.discord.handle.obj.Permissions.BAN);
            case Permissions.MUTE_PERMISSION:
                return permissions.contains(sx.blah.discord.handle.obj.Permissions.VOICE_MUTE_MEMBERS);
            case Permissions.KICK_PERMISSION:
                return permissions.contains(sx.blah.discord.handle.obj.Permissions.KICK);
            case Permissions.UNBAN_PERMISSION:
                return permissions.contains(sx.blah.discord.handle.obj.Permissions.BAN);
            case Permissions.UNMUTE_PERMISSION:
                return permissions.contains(sx.blah.discord.handle.obj.Permissions.VOICE_MUTE_MEMBERS);
            case Permissions.INIT_BOT_PERMISSION:
                return permissions.contains(sx.blah.discord.handle.obj.Permissions.ADMINISTRATOR);
            case Permissions.SETUP_BOT_PERMISSION:
                return permissions.contains(sx.blah.discord.handle.obj.Permissions.ADMINISTRATOR);
            case Permissions.INFO_PERMISSION:
                return permissions.contains(sx.blah.discord.handle.obj.Permissions.SEND_MESSAGES);
            case Permissions.CLEAR_PERMISSION:
                return permissions.contains(sx.blah.discord.handle.obj.Permissions.MANAGE_MESSAGES);
        }
        return false;
    }
}
