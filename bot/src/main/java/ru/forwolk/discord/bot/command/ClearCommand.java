package ru.forwolk.discord.bot.command;

import com.darichey.discord.CommandContext;
import ru.forwolk.discord.bot.Message;
import ru.forwolk.discord.bot.Permissions;
import sx.blah.discord.handle.obj.IChannel;

import java.awt.*;

public class ClearCommand extends AbstractCommand {

    public ClearCommand() {
        super(Permissions.CLEAR_PERMISSION);
    }

    @Override
    public void execute(CommandContext commandContext) {
        IChannel channel = commandContext.getChannel();
        if (commandContext.getArgs().size() > 0) {
            try {
                int num = Integer.parseInt(commandContext.getArgs().get(0));
                channel.getMessageHistory(num + 1).bulkDelete();
            } catch (NumberFormatException e) {
                output.print(channel, Message.INVALID_NUMBER, Color.YELLOW);
            }
        } else {
            output.print(channel, Message.TOO_LOW_ARGUMENTS, Color.YELLOW);
        }
    }
}
