package ru.forwolk.discord.bot.command;

public interface TemporaryCommand {

    default long convertTimeToSec (String time) {
        int timeInt = Integer.parseInt(time.substring(0, time.length() - 1));
        String symbol = time.substring(time.length() - 1);
        switch (symbol.toLowerCase()) {
            case "d":
                timeInt *= 24;
            case "h":
                timeInt *= 60;
            case "m":
                timeInt *= 60;
            case "s":
                break;
            default:
                throw new RuntimeException("Invalid symbol: " + symbol);
        }
        return timeInt;
    }
}
