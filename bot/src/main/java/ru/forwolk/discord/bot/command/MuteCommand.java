package ru.forwolk.discord.bot.command;

import com.darichey.discord.CommandContext;
import lombok.extern.slf4j.Slf4j;
import ru.forwolk.discord.bot.IViolationService;
import ru.forwolk.discord.bot.Message;
import ru.forwolk.discord.bot.Permissions;
import sx.blah.discord.api.internal.json.objects.EmbedObject;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IUser;

import java.awt.*;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class MuteCommand extends AbstractCommand implements TemporaryCommand {

    /**
     * .mute %user% %time% %reason%
     * alias: .m
     */
    public MuteCommand() {
        super(Permissions.MUTE_PERMISSION);
    }

    public void execute(CommandContext commandContext) {
        IChannel channel = commandContext.getChannel();
        List<String> args = commandContext.getArgs();
        if (args.size() < 3) {
            output.print(channel, Message.TOO_LOW_ARGUMENTS, Color.YELLOW);
        } else {
            long bannedUser = parseFromString(args.get(0));
            long time;
            try {
                time = convertTimeToSec(args.get(1));
            } catch (RuntimeException e) {
                output.print(channel, Message.INVALID_TIME_FORMAT, Color.YELLOW);
                log.info("Unable to convert time", e);
                return;
            }
            String reason = args.stream().skip(2).collect(Collectors.joining(" "));
            IUser muted = commandContext.getGuild().getUserByID(bannedUser);
            EmbedObject notification = muteNotification(commandContext.getAuthor().mention(), muted.mention(), reason, new Date(System.currentTimeMillis() + 1000 * time));
            sendMessage(channel, notification);
            muted.getOrCreatePMChannel().sendMessage(notification);
            violationService.mute(commandContext.getGuild().getLongID(), bannedUser, reason, commandContext.getAuthor().getLongID(), time);
        }
    }
}
