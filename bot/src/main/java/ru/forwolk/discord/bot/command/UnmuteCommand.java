package ru.forwolk.discord.bot.command;

import com.darichey.discord.CommandContext;
import lombok.extern.slf4j.Slf4j;
import ru.forwolk.discord.bot.IViolationService;
import ru.forwolk.discord.bot.Message;
import ru.forwolk.discord.bot.Permissions;
import sx.blah.discord.handle.obj.IChannel;

import java.awt.*;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class UnmuteCommand extends AbstractCommand {

    protected UnmuteCommand() {
        super(Permissions.UNMUTE_PERMISSION);
    }

    @Override
    public void execute(CommandContext commandContext) {
        IChannel channel = commandContext.getChannel();
        List<String> args = commandContext.getArgs();
        if (args.size() < 2) {
            output.print(channel, Message.TOO_LOW_ARGUMENTS, Color.YELLOW);
        } else {
            try {
                long bannedUser = parseFromString(args.get(0));
                sendViolationMessage(commandContext, bannedUser, Message.USER_UNMUTED.getMessage(), Color.BLUE);
                violationService.unmute(commandContext.getGuild().getLongID(), bannedUser, commandContext.getAuthor().getLongID(), args.stream().skip(1).collect(Collectors.joining(" ")));
            } catch (NumberFormatException e) {
                log.info("Invalid long {}", args.get(0));
            }
        }
    }
}
