package ru.forwolk.discord.bot.command;

import com.darichey.discord.CommandContext;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import ru.forwolk.discord.bot.IOutputManager;
import ru.forwolk.discord.bot.IPermissionService;
import ru.forwolk.discord.bot.IViolationService;
import ru.forwolk.discord.bot.repository.GuildInfoRepository;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.internal.json.objects.EmbedObject;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.EmbedBuilder;

import java.awt.*;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Slf4j
public abstract class AbstractCommand implements Consumer<CommandContext>{
    @Setter
    protected IPermissionService permissionService;
    @Setter
    protected IOutputManager output;
    @Setter
    protected IViolationService violationService;
    @Setter
    protected IDiscordClient discordClient;
    @Setter
    protected GuildInfoRepository guildInfoRepository;
    @Setter
    protected String version;
    private final static SimpleDateFormat SDF = new SimpleDateFormat("dd.MM.yy HH:mm:ss");


    private final String permission;

    protected AbstractCommand(String permission) {
        this.permission = permission;
    }

    @Override
    public void accept(CommandContext commandContext) {
        IChannel channel = commandContext.getChannel();
        if (allowedCommandInChannel(channel)) {
            IUser user = commandContext.getAuthor();
            if (permissionService.hasPermission(commandContext.getGuild(), user, permission)) {
                log.info("User {} executed command {} {}", user.getName(), commandContext.getName(), commandContext.getArgs().stream().collect(Collectors.joining(" ")));
                execute(commandContext);
            } else {
                output.print(channel, "No permission", Color.RED);
            }
        } else {
            output.print(channel, "Not allowed in this chat", Color.RED);
        }
    }

    public abstract void execute (CommandContext commandContext);

    private boolean allowedCommandInChannel (IChannel channel) {
        //TODO: Implement
        return true;
    }

    protected long parseFromString (String sid) {
        try {
            String e = sid.replaceAll("<", "");
            e = e.replaceAll("!", "");
            e = e.replaceAll("@", "");
            e = e.replaceAll(">", "");
            return Long.parseLong(e);
        } catch (NumberFormatException e) {
            return Long.parseLong(sid);
        }
    }

    protected void sendViolationMessage (CommandContext commandContext, long userId, String messageTemplate, Color color) {
        String actor = commandContext.getAuthor().getName();
        String banned = commandContext.getGuild().getUserByID(userId).getName();
        EmbedBuilder builder = new EmbedBuilder();
        builder.withColor(color);
        builder.withAuthorName(actor);
        builder.appendField("Violation manager", MessageFormat.format(messageTemplate, actor, banned), true);
        builder.withImage("https://pp.userapi.com/c845220/v845220308/4d960/3eJ6Gvnnp-0.jpg");
        commandContext.getChannel().sendMessage(builder.build());
    }

    protected void sendMessage (IChannel channel, EmbedObject message) {
        channel.sendMessage(message);
    }

    protected EmbedObject banNotification (String admin, String muted, String reason, Date until) {
        EmbedBuilder builder = new EmbedBuilder();
        builder.withColor(Color.RED);
        builder.withAuthorName("Уведомление о блокировке");
        builder.appendField("Выдал наказание", admin, true);
        builder.appendField("Наказание получил", muted, true);
        if (reason != null && !reason.isEmpty())
            builder.appendField("Причина", reason, true);
        if (until != null) {
            builder.appendField("Разблокировка", SDF.format(until), false);
        }
        builder.withImage(getBanImage());
        return builder.build();
    }

    protected EmbedObject muteNotification (String admin, String muted, String reason, Date until) {
        EmbedBuilder builder = new EmbedBuilder();
        builder.withColor(Color.RED);
        builder.withAuthorName("Уведомление о блокировке чата");
        builder.appendField("Выдал наказание", admin, true);
        builder.appendField("Наказание получил", muted, true);
        if (reason != null && !reason.isEmpty())
            builder.appendField("Причина", reason, true);
        if (until != null) {
            builder.appendField("Разблокировка", SDF.format(until), false);
        }
        builder.withImage(getMuteImage());
        return builder.build();
    }

    protected EmbedObject kickNotification (String admin, String kicked, String reason) {
        EmbedBuilder builder = new EmbedBuilder();
        builder.withColor(Color.RED);
        builder.withAuthorName("Уведомление о кике с сервера");
        builder.appendField("Выдал наказание", admin, true);
        builder.appendField("Наказание получил", kicked, true);
        if (reason != null && !reason.isEmpty())
            builder.appendField("Причина", reason, true);
        builder.withImage(getKickImage());
        return builder.build();
    }

    private String getBanImage () {
        String[] banImages = new String[] {
                "https://pp.userapi.com/c845220/v845220308/4d960/3eJ6Gvnnp-0.jpg",
                "https://pp.userapi.com/c824203/v824203834/8d934/2NVdIw-WDTI.jpg",
                "https://pp.userapi.com/c845523/v845523754/d678/IVSZujoAWCg.jpg",
                "https://pp.userapi.com/c834302/v834302001/135339/6Y66URUuHlw.jpg"
        };
        return oneOf(banImages);
    }

    private String getMuteImage () {
        String[] muteImages = new String[] {

        };
        return "https://pp.userapi.com/c845220/v845220308/4d960/3eJ6Gvnnp-0.jpg";
    }

    private String getKickImage () {
        String[] kickImages = new String[] {
                "https://sun9-3.userapi.com/c840436/v840436289/75cbd/I3gJI-1NmCA.jpg"
        };
        return oneOf(kickImages);
    }

    private <T> T oneOf (T... ts) {
        if (ts.length == 0)
            return null;
        int i = (int) ((ts.length - 1) * Math.random());
        return ts[i];
    }

}
