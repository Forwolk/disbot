package ru.forwolk.discord.bot;

public class AuditEvent {
    public static final String BAN_AUDIT_EVENT = "BAN_AUDIT_EVENT";
    public static final String UNBAN_AUDIT_EVENT = "UNBAN_AUDIT_EVENT";
    public static final String MUTE_AUDIT_EVENT = "MUTE_AUDIT_EVENT";
    public static final String UNMUTE_AUDIT_EVENT = "UNMUTE_AUDIT_EVENT";
    public static final String KICK_AUDIT_EVENT = "KICK_AUDIT_EVENT";
    public static final String _AUDIT_EVENT = "";
}
