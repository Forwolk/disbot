package ru.forwolk.discord.bot.command;

import com.darichey.discord.CommandContext;
import ru.forwolk.discord.bot.Permissions;
import sx.blah.discord.util.EmbedBuilder;

import java.awt.*;
import java.util.LinkedList;

public class InfoCommand extends AbstractCommand {

    private final long started = System.currentTimeMillis();
    protected InfoCommand() {
        super(Permissions.INFO_PERMISSION);
    }

    @Override
    public void execute(CommandContext commandContext) {
        EmbedBuilder builder = new EmbedBuilder();
        builder.withColor(Color.BLUE);
        builder.withAuthorName("Информация о боте");
        builder.appendField("Автор", "Forwolk", false);
        builder.appendField("Версия", version, true);
        builder.appendField("Uptime", uptimeStr(), true);
        commandContext.getChannel().sendMessage(builder.build());
    }

    private String uptimeStr() {
        long uptime = uptime() / 1000;
        LinkedList<String> ll = new LinkedList<>();
        if(uptime < 60) {
            ll.add(uptime + "s");
        } else {
            ll.add(uptime % 60 + "s");
            uptime /= 60;
            if(uptime < 60) {
                ll.add(uptime + "m ");
            } else {
                ll.add(uptime % 60 + "m ");
                uptime /= 60;
                if(uptime < 24) {
                    ll.add(uptime + "h ");
                } else {
                    ll.add(uptime % 24 + "h ");
                    ll.add(uptime / 24 + "d ");
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        while(!ll.isEmpty()) {
            sb.append(ll.removeLast());
        }
        return sb.toString();
    }

    private long uptime () {
        return System.currentTimeMillis() - started;
    }
}
