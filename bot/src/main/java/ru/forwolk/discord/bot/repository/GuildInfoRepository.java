package ru.forwolk.discord.bot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.forwolk.discord.bot.entity.GuildInfo;

public interface GuildInfoRepository extends JpaRepository<GuildInfo, Long> {
}
