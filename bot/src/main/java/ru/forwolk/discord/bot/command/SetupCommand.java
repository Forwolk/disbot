package ru.forwolk.discord.bot.command;

import com.darichey.discord.CommandContext;
import lombok.extern.slf4j.Slf4j;
import ru.forwolk.discord.bot.Message;
import ru.forwolk.discord.bot.Permissions;
import ru.forwolk.discord.bot.entity.GuildInfo;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.util.EmbedBuilder;

import java.awt.*;
import java.util.List;

@Slf4j
public class SetupCommand extends AbstractCommand {


    protected SetupCommand() {
        super(Permissions.SETUP_BOT_PERMISSION);
    }

    @Override
    public void execute(CommandContext commandContext) {
        IChannel channel = commandContext.getChannel();
        List<String> args = commandContext.getArgs();
        if (args.size() < 1) {
            output.print(channel, Message.TOO_LOW_ARGUMENTS, Color.YELLOW);
        } else {
            switch (args.get(0).toLowerCase()){
                case "log_channel":
                    setup(commandContext, 0);
                    ok(commandContext.getChannel(), "Канал логов настроен");
                    break;
                case "games_channel":
                    setup(commandContext, 1);
                    ok(commandContext.getChannel(), "Канал для игр настроен");
                    break;
            }
        }
    }

    private void ok (IChannel channel, String message) {
        EmbedBuilder builder = new EmbedBuilder();
        builder.withColor(Color.GREEN);
        builder.appendField("Выполнено", message, true);
        channel.sendMessage(builder.build());
    }

    private void setup(CommandContext commandContext, int id) {
        GuildInfo info = guildInfoRepository.findOne(commandContext.getGuild().getLongID());
        if (info == null) {
            info = new GuildInfo();
            info.setGuildId(commandContext.getGuild().getLongID());
        }
        switch (id) {
            case 0:
                info.setLogChannelId(commandContext.getChannel().getLongID());
                break;
            case 1:
                info.setGamesChannelId(commandContext.getChannel().getLongID());
                break;
        }

        guildInfoRepository.save(info);
    }
}
