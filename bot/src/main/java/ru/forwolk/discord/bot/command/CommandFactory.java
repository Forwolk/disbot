package ru.forwolk.discord.bot.command;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import ru.forwolk.discord.bot.IOutputManager;
import ru.forwolk.discord.bot.IPermissionService;
import ru.forwolk.discord.bot.IViolationService;
import ru.forwolk.discord.bot.repository.GuildInfoRepository;
import sx.blah.discord.api.IDiscordClient;

@Slf4j
@AllArgsConstructor
public class CommandFactory {
    private final IPermissionService permissionService;
    private final IOutputManager outputManager;
    private final IViolationService violationService;
    private final IDiscordClient discordClient;
    private final GuildInfoRepository guildInfoRepository;

    @Value("${bot.version}")
    private String version;

    public AbstractCommand createCommand (Class<? extends AbstractCommand> tClass) {
        try {
            AbstractCommand command = tClass.newInstance();
            command.setOutput(outputManager);
            command.setPermissionService(permissionService);
            command.setViolationService(violationService);
            command.setDiscordClient(discordClient);
            command.setGuildInfoRepository(guildInfoRepository);
            command.setVersion(version);
            return command;
        } catch (InstantiationException | IllegalAccessException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }
}
