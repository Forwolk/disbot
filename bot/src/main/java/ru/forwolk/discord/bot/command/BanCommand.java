package ru.forwolk.discord.bot.command;

import com.darichey.discord.CommandContext;
import lombok.extern.slf4j.Slf4j;
import ru.forwolk.discord.bot.IViolationService;
import ru.forwolk.discord.bot.Message;
import ru.forwolk.discord.bot.Permissions;
import sx.blah.discord.api.internal.json.objects.EmbedObject;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IUser;

import java.awt.*;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class BanCommand extends AbstractCommand {

    /**
     * .ban %user% %reason%
     * alias: .b
     */
    public BanCommand() {
        super(Permissions.BAN_PERMISSION);
    }

    public void execute(CommandContext commandContext) {
        IChannel channel = commandContext.getChannel();
        List<String> args = commandContext.getArgs();
        if (args.size() < 2) {
            output.print(channel, Message.TOO_LOW_ARGUMENTS, Color.YELLOW);
        } else {
            try {
                long bannedUser = parseFromString(args.get(0));
                String reason = args.stream().skip(1).collect(Collectors.joining(" "));
                IUser banned = commandContext.getGuild().getUserByID(bannedUser);
                EmbedObject notification = banNotification(commandContext.getAuthor().mention(), banned.mention(), reason, null);
                sendMessage(channel, notification);
                banned.getOrCreatePMChannel().sendMessage(notification);
                violationService.ban(commandContext.getGuild().getLongID(), bannedUser, reason, commandContext.getAuthor().getLongID());
            } catch (NumberFormatException e) {
                log.info("Invalid long {}", args.get(0));
            }
        }
    }
}
