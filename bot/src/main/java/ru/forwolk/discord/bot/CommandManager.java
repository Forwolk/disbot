package ru.forwolk.discord.bot;

import com.darichey.discord.Command;
import com.darichey.discord.CommandListener;
import com.darichey.discord.CommandRegistry;
import lombok.extern.slf4j.Slf4j;
import ru.forwolk.discord.bot.command.*;
import sx.blah.discord.api.IDiscordClient;

@Slf4j
public class CommandManager extends CommandListener {

    private final CommandRegistry commandRegistry;

    public CommandManager(CommandRegistry commandRegistry, CommandFactory commandFactory, IDiscordClient client) {
        super(commandRegistry);
        this.commandRegistry = commandRegistry;
        log.info("Creating command manager...");
        Command banCommand = Command.builder().onCalled(commandFactory.createCommand(BanCommand.class)).build();
        Command tempBanCommand = Command.builder().onCalled(commandFactory.createCommand(TempBanCommand.class)).build();
        Command unbanCommand = Command.builder().onCalled(commandFactory.createCommand(UnbanCommand.class)).build();
        Command muteCommand = Command.builder().onCalled(commandFactory.createCommand(MuteCommand.class)).build();
        Command unmuteCommand = Command.builder().onCalled(commandFactory.createCommand(UnmuteCommand.class)).build();
        Command kickCommand = Command.builder().onCalled(commandFactory.createCommand(KickCommand.class)).build();
        Command initCommand = Command.builder().onCalled(commandFactory.createCommand(InitBotCommand.class)).build();
        Command setupCommand = Command.builder().onCalled(commandFactory.createCommand(SetupCommand.class)).build();
        Command infoCommand = Command.builder().onCalled(commandFactory.createCommand(InfoCommand.class)).build();
        Command clearCommand = Command.builder().onCalled(commandFactory.createCommand(ClearCommand.class)).build();

        register(banCommand, "ban", "b");
        register(tempBanCommand, "tempban", "tb");
        register(unbanCommand, "unban", "ub");
        register(muteCommand, "mute", "m");
        register(unmuteCommand, "unmute", "um");
        register(kickCommand, "kick");
        register(initCommand, "init");
        register(setupCommand, "setup");
        register(infoCommand, "info");
        register(clearCommand, "clear");

        client.getDispatcher().registerListener(this);
        log.info("Command manager created");
    }

    private void register(Command command, String name, String... aliases) {
        log.debug("Command {} is registered", name);
        commandRegistry.register(command, name, aliases);
    }
}
