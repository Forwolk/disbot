package ru.forwolk.discord.bot.impl;

import lombok.extern.slf4j.Slf4j;
import ru.forwolk.discord.bot.IOutputManager;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.util.EmbedBuilder;

import java.awt.*;

@Slf4j
public class OutputManagerImpl implements IOutputManager {
    @Override
    public void print(IChannel channel, String message) {
        print(channel, message, Color.YELLOW);
    }

    @Override
    public void print(IChannel channel, String message, Color color) {
        log.info("Bot sent message to channel {}: {}", channel.getName(), message);
        EmbedBuilder builder = new EmbedBuilder();

        builder.withColor(color);
        builder.appendField("Ошибка", message, true);
        channel.sendMessage(builder.build());
    }
}
