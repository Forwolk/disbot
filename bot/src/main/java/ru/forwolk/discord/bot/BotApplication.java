package ru.forwolk.discord.bot;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import ru.forwolk.commons.metrics.IMonitor;
import ru.forwolk.discord.bot.event.ChatEventListener;
import ru.forwolk.discord.bot.repository.GuildInfoRepository;
import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.ReadyEvent;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.modules.Configuration;

@Slf4j
public class BotApplication {
    private IDiscordClient discordClient;
    private final String token;
    @Getter@Setter
    private IRole mutedRole;
    private GuildInfoRepository guildInfoRepository;
    private final IMonitor monitor;

    public BotApplication(String token, GuildInfoRepository guildInfoRepository, IMonitor monitor) {
        this.token = token;
        this.guildInfoRepository = guildInfoRepository;
        this.monitor = monitor;
    }

    public IDiscordClient login () {
        Message.setTranslated(true);
        log.info("Trying to login...");
        Configuration.AUTOMATICALLY_ENABLE_MODULES = false;
        Configuration.LOAD_EXTERNAL_MODULES = false;
        discordClient = new ClientBuilder().withToken(token).build();

        discordClient.getDispatcher().registerListener(this);
        discordClient.getDispatcher().registerListener(new ChatEventListener(guildInfoRepository, monitor));
        discordClient.login();

        return discordClient;
    }

    @EventSubscriber
    public void afterLogin (ReadyEvent readyEvent) {
        log.info("Bot is ready");
    }

    public IDiscordClient getBot() {
        return discordClient;
    }
}
