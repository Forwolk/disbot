package ru.forwolk.discord.bot.command;

import com.darichey.discord.CommandContext;
import lombok.extern.slf4j.Slf4j;
import ru.forwolk.discord.bot.IViolationService;
import ru.forwolk.discord.bot.Message;
import ru.forwolk.discord.bot.Permissions;
import sx.blah.discord.api.internal.json.objects.EmbedObject;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IUser;

import java.awt.*;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class KickCommand extends AbstractCommand {

    protected KickCommand() {
        super(Permissions.KICK_PERMISSION);
    }

    @Override
    public void execute(CommandContext commandContext) {
        IChannel channel = commandContext.getChannel();
        List<String> args = commandContext.getArgs();
        if (args.size() < 2) {
            output.print(channel, Message.TOO_LOW_ARGUMENTS, Color.YELLOW);
        } else {
            try {
                long bannedUser = parseFromString(args.get(0));
                IUser kicked = commandContext.getGuild().getUserByID(bannedUser);
                String reason = args.stream().skip(1).collect(Collectors.joining(" "));
                EmbedObject notification = kickNotification(commandContext.getAuthor().mention(), kicked.mention(), reason);
                sendMessage(channel, notification);
                kicked.getOrCreatePMChannel().sendMessage(notification);
                violationService.kick(commandContext.getGuild().getLongID(), bannedUser, commandContext.getAuthor().getLongID(), reason);
            } catch (NumberFormatException e) {
                log.info("Invalid long {}", args.get(0));
            }
        }
    }
}
