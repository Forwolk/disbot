package ru.forwolk.discord.bot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.forwolk.discord.bot.entity.Mute;

import java.util.List;

public interface MuteRepository extends JpaRepository<Mute, Long> {
    @Query("select mute from Mute mute where mute.until < current_timestamp ")
    List<Mute> findExpiredMutes();
    Mute findMuteByMutedUserId(long mutedUserId);
}
