package ru.forwolk.discord.bot.command;

import com.darichey.discord.CommandContext;
import ru.forwolk.discord.bot.Permissions;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IRole;

import java.awt.*;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Optional;

public class InitBotCommand extends AbstractCommand {

    protected InitBotCommand() {
        super(Permissions.INIT_BOT_PERMISSION);
    }

    private EnumSet<sx.blah.discord.handle.obj.Permissions> blockedPermissions;

    @Override
    public void execute(CommandContext commandContext) {
        IGuild guild = commandContext.getGuild();
        Optional<IRole> roleOptional = guild.getRolesByName("Muted").stream().findAny();
        IRole role;
        if (roleOptional.isPresent()){
            role = roleOptional.get();
        } else {
            role = guild.createRole();
            role.changeName("Muted");
        }
        guild.getChannels().forEach(channel -> {
            channel.overrideRolePermissions(role, EnumSet.noneOf(sx.blah.discord.handle.obj.Permissions.class), getBlockedPermissions());
        });
        output.print(commandContext.getChannel(), "Bot initialized", Color.GREEN);
    }

    private EnumSet<sx.blah.discord.handle.obj.Permissions> getBlockedPermissions () {
        blockedPermissions = EnumSet.of(
                sx.blah.discord.handle.obj.Permissions.SEND_MESSAGES,
                sx.blah.discord.handle.obj.Permissions.SEND_TTS_MESSAGES,
                sx.blah.discord.handle.obj.Permissions.VOICE_SPEAK,
                sx.blah.discord.handle.obj.Permissions.VOICE_USE_VAD
        );
        return blockedPermissions;
    }
}
