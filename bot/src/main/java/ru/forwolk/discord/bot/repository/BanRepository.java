package ru.forwolk.discord.bot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.forwolk.discord.bot.entity.Ban;

import java.util.List;

public interface BanRepository extends JpaRepository <Ban, Long>{
    @Query("select ban from Ban ban where ban.until < current_timestamp")
    List<Ban> findExpiredBans();
    Ban findBanByBannedUserId(long bannedUserId);
}
