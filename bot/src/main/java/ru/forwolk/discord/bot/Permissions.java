package ru.forwolk.discord.bot;

public class Permissions {

    public static final String BAN_PERMISSION = "bot.violation.ban";
    public static final String UNBAN_PERMISSION = "bot.violation.unban";
    public static final String MUTE_PERMISSION = "bot.violation.mute";
    public static final String UNMUTE_PERMISSION = "bot.violation.unmute";
    public static final String KICK_PERMISSION = "bot.violation.kick";
    public static final String INIT_BOT_PERMISSION = "bot.admin.init";
    public static final String SETUP_BOT_PERMISSION = "bot.admin.setup";
    public static final String INFO_PERMISSION = "bot.common.info";
    public static final String CLEAR_PERMISSION = "bot.util.clear";
}
