package ru.forwolk.discord.bot.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.forwolk.discord.bot.AuditEvent;
import ru.forwolk.discord.bot.IAuditService;
import ru.forwolk.discord.bot.IViolationService;
import ru.forwolk.discord.bot.entity.Ban;
import ru.forwolk.discord.bot.entity.Mute;
import ru.forwolk.discord.bot.repository.BanRepository;
import ru.forwolk.discord.bot.repository.MuteRepository;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.Permissions;

import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@AllArgsConstructor
public class ViolationServiceImpl implements IViolationService {
    private IAuditService auditService;
    private final BanRepository banRepository;
    private final MuteRepository muteRepository;
    private final IDiscordClient discordClient;
    private final static SimpleDateFormat SDF = new SimpleDateFormat("dd.MM.yy HH:mm:ss");


    public void initAutoUnban () {
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                List<Ban> expiredBans = banRepository.findExpiredBans();
                expiredBans.forEach(b -> unban(b.getGuildId(), b.getBannedUserId(), 0, "AUTO_UNBAN"));
                List<Mute> expiredMutes = muteRepository.findExpiredMutes();
                expiredMutes.forEach(m -> unmute(m.getGuildId(), m.getMutedUserId(), 0, "AUTO_UNMUTE"));
            }
        };
        Timer timer = new Timer();

        timer.schedule(task, 0, 30000);
    }

    @Override
    public void ban(long guildId, long userId, String reason, long actor, long timeInSec) {
        Ban ban = new Ban();
        ban.setActorId(actor);
        ban.setBannedUserId(userId);
        ban.setReason(reason);
        ban.setUntil(new Date(System.currentTimeMillis() + timeInSec * 1000));
        ban.setGuildId(guildId);
        synchronized (banRepository) {
            banRepository.save(ban);
        }
        IGuild guild = discordClient.getGuildByID(guildId);
        guild.banUser(userId, reason);
        log.info("User {} is banned", userId);
        audit(guildId, actor, AuditEvent.BAN_AUDIT_EVENT, userId, reason, ban.getUntil());
    }

    @Override
    public void ban(long guildId, long userId, String reason, long actor) {
        Ban ban = new Ban();
        ban.setActorId(actor);
        ban.setBannedUserId(userId);
        ban.setReason(reason);
        ban.setGuildId(guildId);
        synchronized (banRepository) {
            banRepository.save(ban);
        }
        IGuild guild = discordClient.getGuildByID(guildId);
        guild.banUser(userId, reason);
        log.info("User {} is banned", userId);
        audit(guildId, actor, AuditEvent.BAN_AUDIT_EVENT, userId, reason, null);
    }

    @Override
    public void mute(long guildId, long userId, String reason, long actor, long timeInSec) {
        Mute mute = new Mute();
        mute.setActorId(actor);
        mute.setMutedUserId(userId);
        mute.setReason(reason);
        mute.setUntil(new Date(System.currentTimeMillis() + timeInSec * 1000));
        mute.setGuildId(guildId);
        synchronized (muteRepository) {
            muteRepository.save(mute);
        }
        IGuild guild = discordClient.getGuildByID(guildId);
        guild.setMuteUser(guild.getUserByID(userId), true);
        guild.getUserByID(userId).addRole(getMutedRole(guild));
        log.info("User {} is muted", userId);
        audit(guildId, actor, AuditEvent.MUTE_AUDIT_EVENT, userId, reason, mute.getUntil());
    }

    @Override
    public void unban(long guildId, long userId, long actor, String reason) {
        synchronized (banRepository) {
            Optional<Ban> banOptional = Optional.ofNullable(banRepository.findBanByBannedUserId(userId));
            banOptional.ifPresent(ban -> {
                banRepository.delete(ban);
                IGuild guild = discordClient.getGuildByID(guildId);
                guild.pardonUser(userId);
                log.info("User unbanned: {}", ban.getBannedUserId());
                audit(guildId, actor, AuditEvent.UNBAN_AUDIT_EVENT, userId, reason, null);
            });
        }
    }

    @Override
    public void unmute(long guildId, long userId, long actor, String reason) {
        synchronized (muteRepository) {
            Optional<Mute> muteOptional = Optional.ofNullable(muteRepository.findMuteByMutedUserId(userId));
            muteOptional.ifPresent(mute -> {
                muteRepository.delete(mute);
                IGuild guild = discordClient.getGuildByID(guildId);
                guild.setMuteUser(guild.getUserByID(userId), false);
                guild.getUserByID(userId).removeRole(getMutedRole(guild));
                log.info("User unmuted: {}", mute.getMutedUserId());
                audit(guildId, actor, AuditEvent.UNMUTE_AUDIT_EVENT, userId, reason, null);
            });
        }
    }

    @Override
    public void kick(long guildId, long userId, long actor, String reason) {
        IGuild guild = discordClient.getGuildByID(guildId);
        guild.kickUser(guild.getUserByID(userId));
        audit(guildId, actor, AuditEvent.KICK_AUDIT_EVENT, userId, reason, null);
    }

    @Override
    public boolean isMuted(long guildId, long userId) {
        Optional<Mute> muteOptional = Optional.ofNullable(muteRepository.findMuteByMutedUserId(userId));
        if (muteOptional.isPresent()) {
            Mute mute = muteOptional.get();
            if (mute.getUntil().before(new Date())) {
                unmute(guildId, userId, 0, "AUTO_UNMUTE");
                log.info("User {} autounmuted", userId);
                return false;
            }
        }
        return muteOptional.isPresent();
    }

    @Override
    public boolean isBanned(long guildId, long userId) {
        Optional<Ban> banOptional = Optional.ofNullable(banRepository.findBanByBannedUserId(userId));
        if (banOptional.isPresent()) {
            Ban ban = banOptional.get();
            if (ban.getUntil() != null && ban.getUntil().before(new Date())) {
                unban(guildId, userId, 0, "AUTO_UNBAN");
                log.info("User {} autounbanned", userId);
                return false;
            }
        }
        return banOptional.isPresent();
    }

    private void audit (long guildId, long actor, String auditEvent, long user, String reason, Date until) {
        Map<String, String> auditData = new HashMap<>();
        auditData.put("User", String.valueOf(user));
        auditData.put("GuildId", String.valueOf(guildId));
        if (!reason.isEmpty())
            auditData.put("Reason", reason);
        if (until != null) {
            auditData.put("Until", SDF.format(until));
        }
        auditService.audit(actor, auditEvent, auditData);
    }


    private IRole getMutedRole (IGuild guild) {
        Optional<IRole> roleOptional = guild.getRolesByName("Muted").stream().findAny();
        IRole role;
        if (roleOptional.isPresent()){
            role = roleOptional.get();
        } else {
            role = guild.createRole();
            role.changeName("Muted");
        }
        EnumSet<Permissions> permissionsEnumSet = role.getPermissions();
        permissionsEnumSet.remove(sx.blah.discord.handle.obj.Permissions.SEND_MESSAGES);
        permissionsEnumSet.remove(sx.blah.discord.handle.obj.Permissions.SEND_TTS_MESSAGES);
        permissionsEnumSet.remove(sx.blah.discord.handle.obj.Permissions.VOICE_SPEAK);
        permissionsEnumSet.remove(sx.blah.discord.handle.obj.Permissions.VOICE_USE_VAD);
        role.changePermissions(permissionsEnumSet);
        return role;
    }
}
