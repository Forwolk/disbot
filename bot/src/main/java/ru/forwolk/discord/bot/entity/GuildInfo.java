package ru.forwolk.discord.bot.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "disbot_guilds")
@Data
public class GuildInfo {
    @Id
    private long guildId;
    private long logChannelId;
    private long gamesChannelId;
}
