package ru.forwolk.discord.bot.event;

import lombok.Data;
import ru.forwolk.commons.metrics.IMonitor;
import ru.forwolk.discord.bot.entity.GuildInfo;
import ru.forwolk.discord.bot.repository.GuildInfoRepository;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.api.internal.json.objects.EmbedObject;
import sx.blah.discord.handle.impl.events.ReadyEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageUpdateEvent;
import sx.blah.discord.handle.impl.events.guild.member.*;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.EmbedBuilder;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class ChatEventListener {

    private final GuildInfoRepository guildInfoRepository;
    private final IMonitor monitor;
    private final static SimpleDateFormat SDF = new SimpleDateFormat("HH:mm:ss dd:MM:yyyy");

    private final static String MESSAGE_SENT_METRIC = "%s_MESSAGE_SENT_METRIC";
    private final static String USER_JOIN_METRIC = "%s_USER_JOIN_METRIC";
    private final static String USER_LEFT_METRIC = "%s_USER_LEFT_METRIC";
    private final static String BAN_METRIC = "%s_BAN_METRIC";
    private final static String MUTE_METRIC = "%s_MUTE_METRIC";
    private final static String ONLINE_METRIC = "%s_ONLINE_METRIC";
    private final static String TOTAL_METRIC = "%s_TOTAL_ONLINE_METRIC";

    private final int MINUTE = 60000;

    private boolean onlineWriting = false;

    public ChatEventListener(GuildInfoRepository guildInfoRepository, IMonitor monitor) {
        this.guildInfoRepository = guildInfoRepository;
        this.monitor = monitor;
    }

    @EventSubscriber
    public void onEnable (ReadyEvent readyEvent) {
        if (!onlineWriting) {
            onlineWriting = true;
            List<IGuild> guilds = readyEvent.getClient().getGuilds();
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    for (IGuild guild : guilds) {
                        monitor.writeMetric(String.format(TOTAL_METRIC, guild.getName()),guild.getTotalMemberCount());
                    }
                }
            };
            Timer timer = new Timer();
            timer.schedule(timerTask, MINUTE, MINUTE);
        }

    }

    @EventSubscriber
    public void messageEditListener (MessageUpdateEvent event) {
        if (!event.getMessage().getAuthor().isBot()) {
            EventMessage message = new EventMessage();
            message.setTitle(":pencil: Сообщение отредактировано");
            message.setPrincipal(event.getAuthor());
            message.setGuild(event.getGuild());
            message.add("Автор", event.getMessage().getAuthor().getName());
            message.add("Канал", event.getChannel().mention());
            message.add("Страое сообщение", event.getOldMessage().getContent());
            message.add("Новое сообщение", event.getNewMessage().getContent());
            sendToLogChat(message);
        }
    }

    @EventSubscriber
    public void userJoinistener(UserJoinEvent event) {
        EventMessage message = new EventMessage();
        message.setTitle(":white_check_mark: Пользователь подключился");
        message.setPrincipal(event.getUser());
        message.setGuild(event.getGuild());
        monitor.incrementMetric(String.format(USER_JOIN_METRIC, event.getGuild().getName()));
        sendToLogChat(message);
    }

    @EventSubscriber
    public void userBanEvent (UserBanEvent event) {
        EventMessage message = new EventMessage();
        message.setTitle(":no_entry_sign: Пользователь забанен");
        message.setPrincipal(event.getUser());
        message.setGuild(event.getGuild());
        sendToLogChat(message);
    }

   @EventSubscriber
    public void userLeaveListener(UserLeaveEvent event) {
        EventMessage message = new EventMessage();
        message.setTitle(":x: Пользователь отключился");
        message.setPrincipal(event.getUser());
        message.add("Пользователь", event.getUser().getName());
        message.setGuild(event.getGuild());
        monitor.incrementMetric(String.format(USER_LEFT_METRIC, event.getGuild().getName()));
        sendToLogChat(message);
    }

    @EventSubscriber
    public void userNameChangeListener (NicknameChangedEvent event) {
        EventMessage message = new EventMessage();
        message.setTitle(":busts_in_silhouette: Никнейм изменен");
        message.setPrincipal(event.getUser());
        message.setGuild(event.getGuild());
        message.add("Старый ник", event.getOldNickname().orElse(event.getUser().getName()));
        message.add("Новый ник", event.getNewNickname().orElse(event.getUser().getName()));
        sendToLogChat(message);
    }

    @EventSubscriber
    public void userRoleUpdateListener (UserRoleUpdateEvent event) {
        EventMessage message = new EventMessage();
        message.setTitle("⚔ Роли обновлены");
        message.setPrincipal(event.getUser());
        message.setGuild(event.getGuild());
        message.add("Старые роли", rolesToString(event.getOldRoles()));
        message.add("Новые роли", rolesToString(event.getNewRoles()));
        sendToLogChat(message);
    }

    @EventSubscriber
    public void userMessageSend (MessageReceivedEvent event) {
        monitor.incrementMetric(String.format(MESSAGE_SENT_METRIC, event.getGuild().getName()));
    }

    private String rolesToString (List<IRole> roles) {
        return roles.stream().map(IRole::mention).collect(Collectors.joining(", "));
    }

    private void sendToLogChat (EventMessage message) {
        getLogChannel(message.getGuild()).sendMessage(message.message());
    }

    private IChannel getLogChannel (IGuild guild) {
        GuildInfo info = guildInfoRepository.findOne(guild.getLongID());
        if (info == null) {
            return guild.getDefaultChannel();
        } else {
            long chatId = info.getLogChannelId();
            if (chatId != 0)
                return guild.getChannelByID(chatId);
            return guild.getDefaultChannel();
        }
    }

    @Data
    private class EventMessage {
        private IUser principal;
        private Map<String, Object> data = new LinkedHashMap<>();
        private String title;
        private Color color = Color.LIGHT_GRAY;
        private IGuild guild;

        private void add(String key, Object value) {
            data.put(key, value);
        }

        private EmbedObject message () {
            EmbedBuilder message = new EmbedBuilder();
            message.withTitle(this.title);
            message.withColor(this.color);
            message.appendDesc(this.principal.mention());

            for(Map.Entry<String, Object> d : data.entrySet()) {
                if (d.getKey() != null && d.getValue() != null && !d.getKey().isEmpty() && !d.getValue().toString().isEmpty()) {
                    message.appendField(d.getKey(), d.getValue().toString(), true);
                }
            }

            message.withFooterText(SDF.format(new Date()));

            return message.build();
        }
    }
}
