package ru.forwolk.discord.bot.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "disbot_mute")
public class Mute {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private long mutedUserId;
    private long actorId;
    private String reason;
    private Date timestamp = new Date();
    private Date until;
    private long guildId;
}
